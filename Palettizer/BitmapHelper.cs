﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Palettizer
{
    public static class BitmapHelper
    {
        public static BitmapSource LoadPng(ref string path)
        {
            var openfile = new OpenFileDialog();
            openfile.DefaultExt = ".png";
            openfile.Filter = "Image Files (*.png)|*.png";
            openfile.InitialDirectory = Path.GetDirectoryName(path);
            openfile.FileName = Path.GetFileName(path);

            if (!(openfile.ShowDialog() ?? false)) return null;

            path = openfile.FileName;

            using (var stream = openfile.OpenFile())
            {
                return new FormatConvertedBitmap(
                    BitmapDecoder.Create(stream, BitmapCreateOptions.None, BitmapCacheOption.OnLoad).Frames[0], 
                    PixelFormats.Bgra32, null, 0);
            }
        }

        public static string SaveAsPng(this BitmapSource bitmap, string path)
        {
            if (bitmap == null) return null;

            var save = new SaveFileDialog();
            save.DefaultExt = ".png";
            save.InitialDirectory = Path.GetDirectoryName(path);
            save.Filter = "Image File (*.png)|*.png";
            save.FileName = Path.GetFileName(path);

            if (!(save.ShowDialog() ?? false)) return null;

            var encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bitmap));
            using (var stream = save.OpenFile())
            {
                encoder.Save(stream);
            }

            return save.FileName;
        }

        public static Color[,] CopyPixels(this BitmapSource source)
        {
            return source.CopyPixels(new Int32Rect(0, 0, source.PixelWidth, source.PixelHeight));
        }

        public static Color[,] CopyPixels(this BitmapSource source, Int32Rect rect)
        {
            int stride = rect.Width * (source.Format.BitsPerPixel) / 8;
            Color[,] colors = new Color[rect.Width, rect.Height];

            byte[] pixels = new byte[stride * rect.Height];
            source.CopyPixels(rect, pixels, stride, 0);

            for (int i = 0; i < rect.Width; i++)
            {
                for (int j = 0; j < rect.Height; j++)
                {
                    int index = (i + j * rect.Width) * 4;
                    Color c = new Color();
                    c.A = pixels[index + 3];
                    c.R = pixels[index + 2];
                    c.G = pixels[index + 1];
                    c.B = pixels[index + 0];
                    colors[i, j] = c;
                }
            }

            return colors;
        }

        public static void WritePixels(this WriteableBitmap bitmap, Color[,] colors)
        {
            bitmap.WritePixels(colors, new Int32Rect(0, 0, bitmap.PixelWidth, bitmap.PixelHeight));
        }

        public static void WritePixels(this WriteableBitmap bitmap, Color[,] colors, Int32Rect rect)
        {
            int stride = rect.Width * (bitmap.Format.BitsPerPixel) / 8;
            bitmap.WritePixels(rect, ToPixels(colors), stride, 0);
        }

        public static Color[,] CreateBleed(this Color[,] pixels, int bleed)
        {
            Color[,] padded = new Color[pixels.GetLength(0) + bleed * 2, pixels.GetLength(1) + bleed * 2];

            // Copy pixels over offset by bleed
            for (int i = 0; i < pixels.GetLength(0); i++)
            {
                for (int j = 0; j < pixels.GetLength(1); j++)
                {
                    padded[bleed + i, bleed + j] = pixels[i, j];
                }
            }

            // Bleed vertically
            for (int i = 0; i < padded.GetLength(0); i++)
            {
                for (int j = 0; j < bleed; j++)
                {
                    padded[i, bleed - j - 1] = padded[i, bleed];
                    padded[i, padded.GetLength(1) - bleed + j] = padded[i, padded.GetLength(1) - bleed - 1];
                }
            }

            // Bleed horizontally
            for (int i = 0; i < padded.GetLength(1); i++)
            {
                for (int j = 0; j < bleed; j++)
                {
                    padded[bleed - j - 1, i] = padded[bleed, i];
                    padded[padded.GetLength(1) - bleed + j, i] = padded[padded.GetLength(1) - bleed - 1, i];
                }
            }

            return padded;
        }

        public static Color CreateColor(byte r, byte g, byte b, byte a)
        {
            return new Color() { A = a, R = r, G = g, B = b };
        }

        private static byte[] ToPixels(Color[,] colors)
        {
            byte[] pixels = new byte[colors.GetLength(0) * colors.GetLength(1) * 4];
            for (int i = 0; i < colors.GetLength(0); i++)
            {
                for (int j = 0; j < colors.GetLength(1); j++)
                {
                    var color = colors[i, j];
                    int index = (i + colors.GetLength(0) * j) * 4;
                    pixels[index + 3] = color.A;
                    pixels[index + 2] = color.R;
                    pixels[index + 1] = color.G;
                    pixels[index + 0] = color.B;
                }
            }
            return pixels;
        }
    }
}
