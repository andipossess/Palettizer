﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Palettizer
{
    /// <summary>
    /// Interaction logic for PaletteView.xaml
    /// </summary>
    public partial class PaletteView : UserControl
    {
        public static readonly DependencyProperty ColorsProperty = DependencyProperty.Register("Colors", typeof(List<Color>), typeof(PaletteView), new PropertyMetadata(null));
        public List<Color> Colors
        {
            get { return (List<Color>)GetValue(ColorsProperty); }
            set { SetValue(ColorsProperty, value); }
        }

        public PaletteView()
        {
            InitializeComponent();
        }
    }

    public class ColorToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var color = value as Color?;
            return string.Format("A:{0}, R:{1}, G:{2}, B:{3}", color?.A ?? 0, color?.R ?? 0, color?.G ?? 0, color?.B ?? 0);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
